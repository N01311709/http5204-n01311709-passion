﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using http5204_n01311709_passion.Models;
using http5204_n01311709_passion.Models.ViewModels;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace http5204_n01311709_passion.Controllers
{
    public class CreatureController : Controller
    {
        private SINContext db = new SINContext();

        public ActionResult New()
        {
            // GET: ready the model to create a new card using pre-populated Rarities and Colours tables
            NewSpell newcard = new NewSpell();
            newcard.rarities = db.Rarities.ToList();
            newcard.colours = db.Colours.ToList();
            return View(newcard);
        }

        public ActionResult List()
        {
            return View(db.Creatures.ToList());
        }

        public ActionResult Edit(int? id)
        {
            // GET: Ready the view of the card we are editing using the ID
            EditCreature editcard = new EditCreature();
            editcard.creature = db.Creatures.Find(id);
            editcard.rarities = db.Rarities.ToList();
            editcard.colours = db.Colours.ToList();
            if (editcard.creature != null) return View(editcard);
            else return HttpNotFound();
        }

        // SET: POST command for Creating a new card
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string CreatureName_New, double CreatureCMC_New, double CreaturePower_New, double CreatureToughness_New, int CreatureColour_New, int CreatureRarity_New)
        {
            string query = "insert into Creatures (CreatureName, CMC, Power, Toughness,colour_ColourID,rarity_RarityID) " + "values (@name,@cmc,@power,@toughness,@colour,@rarity)";

            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter("@name", CreatureName_New);
            myparams[1] = new SqlParameter("@cmc", CreatureCMC_New);
            myparams[2] = new SqlParameter("@power", CreaturePower_New);
            myparams[3] = new SqlParameter("@toughness", CreatureToughness_New);
            myparams[4] = new SqlParameter("@colour", CreatureColour_New);
            myparams[5] = new SqlParameter("@rarity", CreatureRarity_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        //  SET: POST command for Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string CreatureName_Edit, double CreatureCMC_Edit, double CreaturePower_Edit, double CreatureToughness_Edit, int CreatureColour_Edit, int CreatureRarity_Edit)
        {
            // flagging issues with the id
            if ((id == null) || (db.Creatures.Find(id) == null))
            {
                return HttpNotFound();
            }

            string query = "update Creatures set CreatureName=@name, " +
                "CMC=@cmc, " +
                "Power=@power, " +
                "Toughness=@toughness, " +
                "colour_ColourID=@colour, " +
                "rarity_RarityID=@rarity where CreatureID=@id";

            SqlParameter[] myparams = new SqlParameter[7];
            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@name";
            myparams[0].Value = CreatureName_Edit;

            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@cmc";
            myparams[1].Value = CreatureCMC_Edit;

            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@power";
            myparams[2].Value = CreaturePower_Edit;

            myparams[3] = new SqlParameter();
            myparams[3].ParameterName = "@toughness";
            myparams[3].Value = CreatureToughness_Edit;

            myparams[4] = new SqlParameter();
            myparams[4].ParameterName = "@colour";
            myparams[4].Value = CreatureColour_Edit;

            myparams[5] = new SqlParameter();
            myparams[5].ParameterName = "@rarity";
            myparams[5].Value = CreatureRarity_Edit;

            myparams[6] = new SqlParameter();
            myparams[6].ParameterName = "@id";
            myparams[6].Value = id;
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        // Deleting the card. The GET is impied based on the pathing.
        public ActionResult Delete(int id)
        {
            string query = "delete from Creatures where CreatureID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));
            return RedirectToAction("List");
        }
    }
}