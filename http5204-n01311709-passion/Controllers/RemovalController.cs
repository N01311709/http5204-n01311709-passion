﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using http5204_n01311709_passion.Models;
using http5204_n01311709_passion.Models.ViewModels;
//using System.Diagnostics;

namespace http5204_n01311709_passion.Controllers
{
    public class RemovalController : Controller
    {
        private SINContext db = new SINContext();

        public ActionResult New()
        {
            // GET: ready the model to create a new card using pre-populated Rarities and Colours tables
            NewSpell newcard = new NewSpell();
            newcard.rarities = db.Rarities.ToList();
            newcard.colours = db.Colours.ToList();
            return View(newcard);
        }

        public ActionResult List()
        {
            return View(db.Removals.ToList());
        }

        public ActionResult Edit(int? id)
        {
            // GET: Ready the view of the card we are editing using the ID
            EditRemoval editcard = new EditRemoval();
            editcard.removal = db.Removals.Find(id);
            editcard.rarities = db.Rarities.ToList();
            editcard.colours = db.Colours.ToList();
            if (editcard.removal != null) return View(editcard);
            else return HttpNotFound();
        }

        // SET: POST command for Creating a new card
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string RemovalName_New, double RemovalCMC_New, double RemovalEffectiveness_New, int RemovalColour_New, int RemovalRarity_New)
        {
            string query = "insert into Removals (RemovalName, CMC, Effectiveness,colour_ColourID,rarity_RarityID) " + "values (@name,@cmc,@effectiveness,@colour,@rarity)";

            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@name", RemovalName_New);
            myparams[1] = new SqlParameter("@cmc", RemovalCMC_New);
            myparams[2] = new SqlParameter("@effectiveness", RemovalEffectiveness_New);
            myparams[3] = new SqlParameter("@colour",RemovalColour_New);
            myparams[4] = new SqlParameter("@rarity",RemovalRarity_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        //  SET: POST command for Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? id, string RemovalName_Edit, double RemovalCMC_Edit, double RemovalEffectiveness_Edit, int RemovalColour_Edit, int RemovalRarity_Edit)
        {
            // flagging issues with the id
            if ((id == null) || (db.Removals.Find(id) == null)) {
                return HttpNotFound();
            }

            string query = "update Removals set RemovalName=@name, "  +
                "CMC=@cmc, " +
                "Effectiveness=@effectiveness, " +
                "colour_ColourID=@colour, " +
                "rarity_RarityID=@rarity where RemovalID=@id";

            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@name";
            myparams[0].Value = RemovalName_Edit;

            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@cmc";
            myparams[1].Value = RemovalCMC_Edit;

            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@effectiveness";
            myparams[2].Value = RemovalEffectiveness_Edit;

            myparams[3] = new SqlParameter();
            myparams[3].ParameterName = "@colour";
            myparams[3].Value = RemovalColour_Edit;

            myparams[4] = new SqlParameter();
            myparams[4].ParameterName = "@rarity";
            myparams[4].Value = RemovalRarity_Edit;

            myparams[5] = new SqlParameter();
            myparams[5].ParameterName = "@id";
            myparams[5].Value = id;
            db.Database.ExecuteSqlCommand(query, myparams);

            //Debug.WriteLine(query, myparams);
            return RedirectToAction("List");
        }

        // Deleting the card. The GET is impied based on the pathing.
        public ActionResult Delete(int id)
        {
            string query = "delete from Removals where RemovalID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));
            return RedirectToAction("List");
        }
    }
}