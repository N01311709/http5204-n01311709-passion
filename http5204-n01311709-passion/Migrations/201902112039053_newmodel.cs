namespace http5204_n01311709_passion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newmodel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Removals", "colour_ColourID", "dbo.Colours");
            DropForeignKey("dbo.Removals", "rarity_RarityID", "dbo.Rarities");
            DropIndex("dbo.Removals", new[] { "colour_ColourID" });
            DropIndex("dbo.Removals", new[] { "rarity_RarityID" });
            AlterColumn("dbo.Removals", "colour_ColourID", c => c.Int(nullable: false));
            AlterColumn("dbo.Removals", "rarity_RarityID", c => c.Int(nullable: false));
            CreateIndex("dbo.Removals", "colour_ColourID");
            CreateIndex("dbo.Removals", "rarity_RarityID");
            AddForeignKey("dbo.Removals", "colour_ColourID", "dbo.Colours", "ColourID", cascadeDelete: true);
            AddForeignKey("dbo.Removals", "rarity_RarityID", "dbo.Rarities", "RarityID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Removals", "rarity_RarityID", "dbo.Rarities");
            DropForeignKey("dbo.Removals", "colour_ColourID", "dbo.Colours");
            DropIndex("dbo.Removals", new[] { "rarity_RarityID" });
            DropIndex("dbo.Removals", new[] { "colour_ColourID" });
            AlterColumn("dbo.Removals", "rarity_RarityID", c => c.Int());
            AlterColumn("dbo.Removals", "colour_ColourID", c => c.Int());
            CreateIndex("dbo.Removals", "rarity_RarityID");
            CreateIndex("dbo.Removals", "colour_ColourID");
            AddForeignKey("dbo.Removals", "rarity_RarityID", "dbo.Rarities", "RarityID");
            AddForeignKey("dbo.Removals", "colour_ColourID", "dbo.Colours", "ColourID");
        }
    }
}
