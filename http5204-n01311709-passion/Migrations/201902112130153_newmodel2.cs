namespace http5204_n01311709_passion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newmodel2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Removals", "Effectiveness", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Removals", "Effectiveness", c => c.Int(nullable: false));
        }
    }
}
