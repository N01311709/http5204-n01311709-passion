namespace http5204_n01311709_passion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newmodel5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Creatures", "CMC", c => c.Double(nullable: false));
            AlterColumn("dbo.Creatures", "Power", c => c.Double(nullable: false));
            AlterColumn("dbo.Creatures", "Toughness", c => c.Double(nullable: false));
            AlterColumn("dbo.Creatures", "Drawback", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Creatures", "Drawback", c => c.Int(nullable: false));
            AlterColumn("dbo.Creatures", "Toughness", c => c.Int(nullable: false));
            AlterColumn("dbo.Creatures", "Power", c => c.Int(nullable: false));
            AlterColumn("dbo.Creatures", "CMC", c => c.Int(nullable: false));
        }
    }
}
