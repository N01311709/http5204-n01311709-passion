namespace http5204_n01311709_passion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Colours",
                c => new
                    {
                        ColourID = c.Int(nullable: false, identity: true),
                        ColourName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.ColourID);
            
            CreateTable(
                "dbo.Creatures",
                c => new
                    {
                        CreatureID = c.Int(nullable: false, identity: true),
                        CreatureName = c.String(nullable: false, maxLength: 255),
                        CMC = c.Int(nullable: false),
                        Power = c.Int(nullable: false),
                        Toughness = c.Int(nullable: false),
                        Drawback = c.Int(nullable: false),
                        colour_ColourID = c.Int(),
                        rarity_RarityID = c.Int(),
                    })
                .PrimaryKey(t => t.CreatureID)
                .ForeignKey("dbo.Colours", t => t.colour_ColourID)
                .ForeignKey("dbo.Rarities", t => t.rarity_RarityID)
                .Index(t => t.colour_ColourID)
                .Index(t => t.rarity_RarityID);
            
            CreateTable(
                "dbo.Rarities",
                c => new
                    {
                        RarityID = c.Int(nullable: false, identity: true),
                        RarityName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.RarityID);
            
            CreateTable(
                "dbo.Removals",
                c => new
                    {
                        RemovalID = c.Int(nullable: false, identity: true),
                        RemovalName = c.String(nullable: false, maxLength: 255),
                        CMC = c.Int(nullable: false),
                        Effectiveness = c.Int(nullable: false),
                        colour_ColourID = c.Int(),
                        rarity_RarityID = c.Int(),
                    })
                .PrimaryKey(t => t.RemovalID)
                .ForeignKey("dbo.Colours", t => t.colour_ColourID)
                .ForeignKey("dbo.Rarities", t => t.rarity_RarityID)
                .Index(t => t.colour_ColourID)
                .Index(t => t.rarity_RarityID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Removals", "rarity_RarityID", "dbo.Rarities");
            DropForeignKey("dbo.Removals", "colour_ColourID", "dbo.Colours");
            DropForeignKey("dbo.Creatures", "rarity_RarityID", "dbo.Rarities");
            DropForeignKey("dbo.Creatures", "colour_ColourID", "dbo.Colours");
            DropIndex("dbo.Removals", new[] { "rarity_RarityID" });
            DropIndex("dbo.Removals", new[] { "colour_ColourID" });
            DropIndex("dbo.Creatures", new[] { "rarity_RarityID" });
            DropIndex("dbo.Creatures", new[] { "colour_ColourID" });
            DropTable("dbo.Removals");
            DropTable("dbo.Rarities");
            DropTable("dbo.Creatures");
            DropTable("dbo.Colours");
        }
    }
}
