namespace http5204_n01311709_passion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newmodel3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Removals", "CMC", c => c.Double(nullable: false));
            AlterColumn("dbo.Removals", "Effectiveness", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Removals", "Effectiveness", c => c.Single(nullable: false));
            AlterColumn("dbo.Removals", "CMC", c => c.Single(nullable: false));
        }
    }
}
