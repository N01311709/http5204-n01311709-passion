namespace http5204_n01311709_passion.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newdb1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Creatures", "colour_ColourID", "dbo.Colours");
            DropForeignKey("dbo.Creatures", "rarity_RarityID", "dbo.Rarities");
            DropIndex("dbo.Creatures", new[] { "colour_ColourID" });
            DropIndex("dbo.Creatures", new[] { "rarity_RarityID" });
            AlterColumn("dbo.Creatures", "colour_ColourID", c => c.Int(nullable: false));
            AlterColumn("dbo.Creatures", "rarity_RarityID", c => c.Int(nullable: false));
            CreateIndex("dbo.Creatures", "colour_ColourID");
            CreateIndex("dbo.Creatures", "rarity_RarityID");
            AddForeignKey("dbo.Creatures", "colour_ColourID", "dbo.Colours", "ColourID", cascadeDelete: true);
            AddForeignKey("dbo.Creatures", "rarity_RarityID", "dbo.Rarities", "RarityID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Creatures", "rarity_RarityID", "dbo.Rarities");
            DropForeignKey("dbo.Creatures", "colour_ColourID", "dbo.Colours");
            DropIndex("dbo.Creatures", new[] { "rarity_RarityID" });
            DropIndex("dbo.Creatures", new[] { "colour_ColourID" });
            AlterColumn("dbo.Creatures", "rarity_RarityID", c => c.Int());
            AlterColumn("dbo.Creatures", "colour_ColourID", c => c.Int());
            CreateIndex("dbo.Creatures", "rarity_RarityID");
            CreateIndex("dbo.Creatures", "colour_ColourID");
            AddForeignKey("dbo.Creatures", "rarity_RarityID", "dbo.Rarities", "RarityID");
            AddForeignKey("dbo.Creatures", "colour_ColourID", "dbo.Colours", "ColourID");
        }
    }
}
