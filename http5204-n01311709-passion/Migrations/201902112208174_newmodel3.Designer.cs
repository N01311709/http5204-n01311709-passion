// <auto-generated />
namespace http5204_n01311709_passion.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class newmodel3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(newmodel3));
        
        string IMigrationMetadata.Id
        {
            get { return "201902112208174_newmodel3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
