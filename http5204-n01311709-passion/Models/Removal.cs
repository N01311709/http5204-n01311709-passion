﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace http5204_n01311709_passion.Models
{
    public class Removal
    {
        //field that describes the removal ID
        [Key,ScaffoldColumn(false)]
        public int RemovalID { get; set; }

        //field that describes the removal Name
        [Required, StringLength(255), Display(Name = "Name")]
        public string RemovalName { get; set; }

        [Required, Display(Name ="Colour")]
        //field that describes the removal Colour
        public virtual Colour colour { get; set; }

        [Required, Display(Name ="Rarity")]
        //field that describes the removal Rarity
        public virtual Rarity rarity { get; set; }

        [Required]
        //field that describes the removal Cost
        public double CMC { get; set; }
        
        ////field that describes the removal Speed — would need to recompute the field
        //[Required, StringLength(255), Display(Name = "Name")]
        //public string Speed { get; set; }

        ////field that describes the removal Damage
        //public int Damage { get; set; }

        ////field that describes the removal Restriction
        //public int Restriction { get; set; }

        [Required]
        //field that describes the removal Effectiveness
        //field that describes the removal Effectiveness
        public double Effectiveness { get; set; }

        ////field that describes the removal CA
        //public int CardA { get; set; }
    }
}