﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace http5204_n01311709_passion.Models
{
    public class Colour
    {
        [Key, ScaffoldColumn(false)]
        public int ColourID { get; set; }

        [Required, StringLength(255), Display(Name = "Colour")]
        public string ColourName { get; set; }
    }
}