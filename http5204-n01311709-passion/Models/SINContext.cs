﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace http5204_n01311709_passion.Models
{
    public class SINContext : DbContext
    {
        public SINContext()
        {

        }
        
        public DbSet<Colour> Colours { get; set; }
        public DbSet<Creature> Creatures { get; set; }
        public DbSet<Rarity> Rarities { get; set; }
        public DbSet<Removal> Removals { get; set; }
    }
}