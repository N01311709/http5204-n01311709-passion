﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace http5204_n01311709_passion.Models
{
    public class Creature
    {
        // field that describes the Creature ID
        [Key, ScaffoldColumn(false)]
        public int CreatureID { get; set; }

        // field that describes the creature name
        [Required, StringLength(255), Display(Name = "Name")]
        public string CreatureName { get; set; }

        // field that describes Colour
        [Required, Display(Name = "Colour")]
        public virtual Colour colour { get; set; }

        // field that describes Rarity 
        [Required, Display(Name = "Rarity")]
        public virtual Rarity rarity { get; set; }

        // field that describes CMC 
        [Required]
        public double CMC { get; set; }

        // field that describes Power 
        [Required]
        public double Power { get; set; }

        // field that describes Toughness
        [Required]
        public double Toughness { get; set; }

        // field that describes Alt CMC
        //public int AltCMC { get; set; }

        //// field that describes Alt Power
        //public int AltPower { get; set; }

        //// field that describes Alt Toughness
        //public int AltToughness { get; set; }

        //// field that describes Evasion 
        //public int Evasion { get; set; }

        //// field that describes Utility 
        //public int Utility { get; set; }

        //// field that describes CA 
        //public int CardA { get; set; }

        // field that describes Drawback
        //public double Drawback { get; set; }
    }
}