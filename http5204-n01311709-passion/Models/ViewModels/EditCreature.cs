﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace http5204_n01311709_passion.Models.ViewModels
{
    public class EditCreature
    {
        public EditCreature()
        {

        }

        public virtual Creature creature { get; set; }

        public IEnumerable<Rarity> rarities { get; set; }

        public IEnumerable<Colour> colours { get; set; }
    }
}