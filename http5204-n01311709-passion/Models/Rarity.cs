﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace http5204_n01311709_passion.Models
{
    public class Rarity
    {
        [Key, ScaffoldColumn(false)]
        public int RarityID { get; set; }

        [Required, StringLength(255), Display(Name = "Rarity")]
        public string RarityName { get; set; }
    }
}