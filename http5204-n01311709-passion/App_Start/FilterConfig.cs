﻿using System.Web;
using System.Web.Mvc;

namespace http5204_n01311709_passion
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
